using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trash : MonoBehaviour
{
    [SerializeField] private ScrapTier[] scrapTiers;
    // Time to spawn new scrap after it is empty
    [SerializeField] private float respawnCooldown = 10f;
    // Time to spawn new scrap when the scrap isn't taken
    [SerializeField] private float refreshTime = 30f;

    [Header("UI")]
    [SerializeField] private Animator animator;
    [SerializeField] private Slider refreshTimeSlider;
    [SerializeField] private Image sliderProgressBarImage;
    [SerializeField] private Image sliderBackgroundBarImage;
    [SerializeField] private Image scrapImage;
    [SerializeField] private Image bubleDotImage;
    [SerializeField] private Color fullTimeColor = Color.green;
    [SerializeField] private Color emptyTimeColor = Color.red;

    private float currentRefreshTime;
    private Scrap currentScrap;
    private bool picked = false;
    private WaitForSeconds waitForRespawnTime;
    private GameManager gameManager;

    private void Start()
    {
        refreshTimeSlider.minValue = 0f;
        refreshTimeSlider.maxValue = refreshTime;
        refreshTimeSlider.value = refreshTime;
        sliderProgressBarImage.color = fullTimeColor;
        sliderBackgroundBarImage.color = Color.Lerp(fullTimeColor, Color.gray, 0.5f);
        currentRefreshTime = refreshTime;
        waitForRespawnTime = new WaitForSeconds(respawnCooldown);
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        SpawnNewScrap();
    }

    private void Update()
    {
        if (!picked && currentRefreshTime <= 0f)
        {
            SpawnNewScrap();
        }

        UpdateTrashUI();
        if (!gameManager.Pause) currentRefreshTime -= Time.deltaTime;
    }


    #region private methods

    private void SpawnNewScrap()
    {
        ScrapTier randomTier = scrapTiers[Random.Range(0, scrapTiers.Length)];
        currentScrap = ScrapManager.GetRandomScrapOfTier(randomTier);
        currentRefreshTime = refreshTime;
        UpdateTrashUI();
    }

    private void UpdateTrashUI()
    {
        if (currentScrap == null)
        {
            if (animator.GetBool("Appear")) animator.SetBool("Appear", false);
            return;
        }

        refreshTimeSlider.value = currentRefreshTime;

        scrapImage.sprite = currentScrap.Sprite;
        sliderProgressBarImage.color = Color.Lerp(emptyTimeColor, fullTimeColor, currentRefreshTime / refreshTime);
        sliderBackgroundBarImage.color = Color.Lerp(sliderProgressBarImage.color, Color.gray, 0.5f);
        bubleDotImage.color = sliderProgressBarImage.color;
        if (!animator.GetBool("Appear")) animator.SetBool("Appear", true);
    }

    #endregion


    #region Coroutines

    private IEnumerator SpawnNewScrapAfterPick()
    {
        picked = true;
        yield return waitForRespawnTime;
        SpawnNewScrap();
        picked = false;
    }

    #endregion

    public Scrap PickScrap()
    {
        if (currentScrap is null || picked) return null;

        Scrap scrap = currentScrap;
        currentScrap = null;
        UpdateTrashUI();
        StartCoroutine(SpawnNewScrapAfterPick());
        return scrap;
    }
}
