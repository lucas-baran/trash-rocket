using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private float gameDuration = 180;
    [SerializeField] private GameObject defeatScreen;
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject buttonCanvas;
    [SerializeField] private Slider timerSlider;
    [SerializeField] private Text scoreText;
    [SerializeField] private GameObject newHighScoreText;

    private float timeRemaining;
    private PlayerController player;
    private bool pause = false;
    private bool tuto = true;

    public bool Pause { get => pause; }

    private GameObject pauseTextsGO;

    private void Start()
    {
        pauseTextsGO = buttonCanvas.transform.Find("PauseTexts").gameObject;
        timeRemaining = gameDuration;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (!tuto)
        {
            if (timeRemaining <= 0)
            {
                defeat();
            }

            timerSlider.value = Mathf.Clamp01(timeRemaining / gameDuration);
            if (!pause)
                timeRemaining -= Time.deltaTime;
        }
    }

    public void defeat()
    {
        GetComponent<AudioSource>().mute = true;
        player.setPlay(false);
        defeatScreen.SetActive(true);
        buttonCanvas.SetActive(true);
        pauseTextsGO.SetActive(false);
        endCameraAnimation();
    }

    public void win()
    {
        GetComponent<AudioSource>().mute = true;
        player.setPlay(false);
        winScreen.SetActive(true);
        buttonCanvas.SetActive(true);
        endCameraAnimation();
        pause = true;
        pauseTextsGO.SetActive(false);
        scoreText.text = "Your score : " + ((int)(gameDuration - timeRemaining)).ToString();

        if(PlayerPrefs.HasKey("Highscore"))
        {
            if(gameDuration - timeRemaining < PlayerPrefs.GetInt("Highscore"))
            {
                newHighScoreText.SetActive(true);
                PlayerPrefs.SetInt("Highscore", (int)(gameDuration - timeRemaining));
            }
        }
        else
        {
            newHighScoreText.SetActive(true);
            PlayerPrefs.SetInt("Highscore", (int)(gameDuration - timeRemaining));
        }
    }

    public void setPause(bool _pause)
    {
        pause = _pause;
        pauseTextsGO.SetActive(pause);
        buttonCanvas.SetActive(pause);
    }

    private void endCameraAnimation()
    {
        GameObject.Find("Main Camera").gameObject.GetComponent<CameraController>().endGame();
    }

    public void EndTuto(bool skiped = false)
    {
        tuto = false;
        player.endTuto(skiped);
        player.setPlay(true);
        timerSlider.gameObject.SetActive(true);
    }

}
