using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rocket : MonoBehaviour
{
    [SerializeField] private SpriteRenderer[] rocketSpriteRenderers;
    [SerializeField] private RequiredScraps[] requiredScrapTiers;

    [Header("Particle system")]
    [SerializeField] private ParticleSystem particles;
    private ParticleSystemRenderer particlesRenderer;

    [Header("UI")]
    [SerializeField] private GridLayoutGroup rocketUIGripLayout;

    [Header("Sounds")]
    [SerializeField] private AudioSource buildingRocketAudioSource;

    [System.Serializable]
    public struct RequiredScraps
    {
        public ScrapTier scrapTier;
        public int number;
    }

    private List<Scrap> requiredScraps = new List<Scrap>();
    private int requiredScrapsStartCount;
    private Image[] rocketUIScraps;

    private GameManager gameManager;

    private void Start()
    {
        particlesRenderer = particles.GetComponent<ParticleSystemRenderer>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        for (int i = 0; i < requiredScrapTiers.Length; i++)
        {
            for (int n = 0; n < requiredScrapTiers[i].number; n++)
            {
                requiredScraps.Add(ScrapManager.GetRandomScrapOfTier(requiredScrapTiers[i].scrapTier));
            }
        }

        rocketUIScraps = rocketUIGripLayout.GetComponentsInChildren<Image>();

        requiredScrapsStartCount = requiredScraps.Count;
        UpdateRocketSprites();
        UpdateRocketUI();
    }

    #region private methods

    private void UpdateRocketUI()
    {
        for (int i = 0; i < rocketUIScraps.Length; i++)
        {
            if (i < requiredScraps.Count)
            {
                rocketUIScraps[i].sprite = requiredScraps[i].Sprite;
                rocketUIScraps[i].enabled = true;
            }
            else
            {
                rocketUIScraps[i].enabled = false;
            }
        }
    }

    private void UpdateRocketSprites()
    {
        float remainingRatio = (float)requiredScraps.Count / requiredScrapsStartCount;

        for (int i = 0; i < rocketSpriteRenderers.Length; i++)
        {
            rocketSpriteRenderers[i].enabled = false;
        }

        for (int i = 0; i < rocketSpriteRenderers.Length; i++)
        {
            if (remainingRatio > 1 - (float)(i + 2) / rocketSpriteRenderers.Length &&  remainingRatio <= 1 - (float)(i + 1) / rocketSpriteRenderers.Length)
            {
                rocketSpriteRenderers[i].enabled = true;
                break;
            }
        }
    }

    #endregion


    public void AddScrap(Scrap scrap)
    {
        if (scrap == null) return;

        buildingRocketAudioSource.Play();
        particlesRenderer.material.mainTexture = scrap.Sprite.texture;
        particles.Play();
        requiredScraps.Remove(scrap);
        UpdateRocketUI();
        UpdateRocketSprites();

        if (requiredScraps.Count == 0)
        {
            //rocket is finished
            gameManager.win(); //TODO Debug ?
        }
    }

    public void AddScraps(Scrap[] scraps)
    {
        if (scraps.Length == 0) return;

        buildingRocketAudioSource.Play();
        particlesRenderer.material.mainTexture = scraps[0].Sprite.texture;
        particles.Play();
        foreach (Scrap scrap in scraps)
        {
            requiredScraps.Remove(scrap);
        }
        UpdateRocketUI();
        UpdateRocketSprites();

        if (requiredScraps.Count == 0)
        {
            //rocket is finished
            gameManager.win(); //TODO Debug ?
        }
    }
}
