using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New scrap", menuName = "ScriptableObjects/Scrap", order = 1)]
public class Scrap : ScriptableObject
{
    public string Name;
    public ScrapTier ScrapTier;
    public Sprite Sprite;
}
