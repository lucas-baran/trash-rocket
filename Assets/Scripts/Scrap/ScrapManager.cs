using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ScrapManager : MonoBehaviour
{
    [SerializeField] private Scrap[] scraps;

    private Dictionary<ScrapTier, List<Scrap>> scrapsPerTier = new Dictionary<ScrapTier, List<Scrap>>();

    #region Singleton

    private static ScrapManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            for (int i = 0; i < scraps.Length; i++)
            {
                if (!scrapsPerTier.ContainsKey(scraps[i].ScrapTier))
                {
                    scrapsPerTier.Add(scraps[i].ScrapTier, new List<Scrap>());
                }

                scrapsPerTier[scraps[i].ScrapTier].Add(scraps[i]);
            }
        }
        else Destroy(this);
    }

    #endregion

    public static Scrap GetRandomScrapOfTier(ScrapTier scrapTier)
    {
        if (!instance.scrapsPerTier.ContainsKey(scrapTier))
        {
            return null;
        }

        return instance.scrapsPerTier[scrapTier][Random.Range(0, instance.scrapsPerTier[scrapTier].Count)];
    }
}
