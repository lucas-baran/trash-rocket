using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutoManager : MonoBehaviour
{
    [SerializeField] private Text tutoText1;
    [SerializeField] private Text tutoText2;
    [SerializeField] private Text tutoText3;
    [SerializeField] private Text tutoText4;
    [SerializeField] private Text tutoText5;
    [SerializeField] private Text tutoText6;
    [SerializeField] private float delay = 0.05f;
    [SerializeField] private float timeToRead = 3;
    [SerializeField] private GameManager gameManager;

    private bool isTyping = false;
    private bool isTuto = true;
    private int currentTuto = 0;
    private bool canPass = false;
    private WaitForSeconds waitForTimeToRead;
    private WaitForSeconds waitForTextDelay;
    private PlayerController playerController;

    private void Start()
    {
        playerController = GameObject.Find("PlayerCharacter").GetComponent<PlayerController>();
        waitForTimeToRead = new WaitForSeconds(timeToRead);
        waitForTextDelay = new WaitForSeconds(delay);
        StartCoroutine(tutoCoroutine());
    }

    private void Update()
    {
        //if(currentTuto == 4 || currentTuto == 5)
        //{
        //    if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown("joystick button 0"))
        //    {
        //        canPass = true;
        //    }
        //}

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 1"))
        {
            gameManager.EndTuto(true);
            isTuto = false;
            this.gameObject.SetActive(false);
        }
    }

    private IEnumerator tutoCoroutine()
    {
        StartCoroutine(typewriterEffectCoroutine(tutoText1));
        while (isTyping)
        {
            yield return null; // Wait justqu'� la prochaine frame
        }
        yield return waitForTimeToRead;
        tutoText1.gameObject.SetActive(false);

        StartCoroutine(typewriterEffectCoroutine(tutoText2));
        while (isTyping)
        {
            yield return null;
        }
        yield return waitForTimeToRead;
        tutoText2.gameObject.SetActive(false);

        StartCoroutine(typewriterEffectCoroutine(tutoText3));
        while (isTyping)
        {
            yield return null;
        }
        yield return waitForTimeToRead;
        tutoText3.gameObject.SetActive(false);

        StartCoroutine(typewriterEffectCoroutine(tutoText4));
        while (isTyping)
        {
            yield return null;
        }
        playerController.setPlay(true);
        currentTuto = 4;
        while (!canPass)
        {
            yield return null;
        }
        tutoText4.gameObject.SetActive(false);
        canPass = false;
        currentTuto = 0;

        StartCoroutine(typewriterEffectCoroutine(tutoText5));
        while (isTyping)
        {
            yield return null;
        }
        currentTuto = 5;
        while (!canPass)
        {
            yield return null;
        }
        tutoText5.gameObject.SetActive(false);
        canPass = false;

        StartCoroutine(typewriterEffectCoroutine(tutoText6));
        while (isTyping)
        {
            yield return null;
        }
        yield return waitForTimeToRead;
        tutoText6.gameObject.SetActive(false);

        gameManager.EndTuto();
        isTuto = false;
        this.gameObject.SetActive(false);
    }


    private IEnumerator typewriterEffectCoroutine(Text tutoText)
    {
        isTyping = true;
        string fullContent = tutoText.text;
        string currentText = "";
        tutoText.text = "";
        tutoText.gameObject.SetActive(true);

        for(int k = 0; k < fullContent.Length; k++)
        {
            currentText += fullContent[k];
            tutoText.text = currentText;
            yield return waitForTextDelay;
        }
        isTyping = false;
    }

    public bool IsTuto()
    {
        return isTuto;
    }

    public void setPassToTrue()
    {
        canPass = true;
    }

    public int getCurrentTuto()
    {
        return currentTuto;
    }
}
