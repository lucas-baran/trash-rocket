using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Openorclose : MonoBehaviour
{
    private float rand;
    public float refreshtime = 4;
    private float time;
    public Sprite Open;
    public Sprite Close;

    private SpriteRenderer objectRenderer;

    void Start()
    {
        objectRenderer = GetComponent<SpriteRenderer>();
        time = 0;
        InvokeRepeating(nameof(Timer), 0f, refreshtime*Time.deltaTime);
    }

    void Timer()
    {
        var it = Random.Range(2, 10);
        rand = it;
    }
    void Update()
    {
        if (time <= 0 )
        {
            time = rand;
            if (gameObject.CompareTag("Killer"))
            {
                transform.gameObject.tag = "Untagged";
                objectRenderer.sprite = Close;
            }
            else
            {
                transform.gameObject.tag = "Killer";
                objectRenderer.sprite = Open;
            }
        }
        else
        {
            time -= 1*Time.deltaTime;
        }
        
    }
}
