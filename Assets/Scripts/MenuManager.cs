using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject creditsMenu;
    [SerializeField] GameObject controlsMenu;
    [SerializeField] Button playButton;
    [SerializeField] Button quitButton;
    [SerializeField] Button creditsButton;
    [SerializeField] Button controlsButton;
    [SerializeField] Button returnButton1;
    [SerializeField] Button returnButton2;
    [SerializeField] GameObject highscoreText;

    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            creditsButton.onClick.AddListener(() => viewCredits());
            controlsButton.onClick.AddListener(() => viewControls());
            returnButton2.onClick.AddListener(() => backToMainMenu());
        }
        playButton.onClick.AddListener(() => launchGame());
        quitButton.onClick.AddListener(() => quitGame());
        returnButton1.onClick.AddListener(() => backToMainMenu());


        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            if (PlayerPrefs.HasKey("Highscore"))
            {
                highscoreText.GetComponent<Text>().text = PlayerPrefs.GetInt("Highscore").ToString();
            }
            else
            {
                highscoreText.GetComponent<Text>().text = "0";
            }
        }
    }

    public void launchGame()
    {
        SceneManager.LoadScene(1); //mettre le bon num�ro
    }

    public void quitGame()
    {
        Application.Quit();
    }

    private void viewCredits()
    {
        mainMenu.SetActive(false);
        creditsMenu.SetActive(true);
    }

    private void viewControls()
    {
        mainMenu.SetActive(false);
        controlsMenu.SetActive(true);
    }

    public void backToMainMenu()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            creditsMenu.SetActive(false);
            controlsMenu.SetActive(false);
            mainMenu.SetActive(true);
        }
        else
            SceneManager.LoadScene(0);
    }
}
