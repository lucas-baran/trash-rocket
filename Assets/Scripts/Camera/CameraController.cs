using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private float speedEndAnimation;

    private bool end = false;
    private Vector2 playerPosition;
    private WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();

    // Update is called once per frame
    void Update()
    {
        if (!end)
        {
            playerPosition = new Vector2(player.transform.position.x, player.transform.position.y);
            transform.position = new Vector3(Mathf.Clamp(playerPosition.x, -3.37f, 3.37f), Mathf.Clamp(playerPosition.y, -6.5f, 6.5f), transform.position.z);
        }
    }

    public void endGame()
    {
        end = true;
        StartCoroutine(EndCoroutine());
    }

    IEnumerator EndCoroutine()
    {
        float step = (speedEndAnimation / (transform.position.magnitude)) * Time.fixedDeltaTime;
        float t = 0;
        Vector3 a = transform.position;
        Vector3 b = new Vector3(3.17f, 1.28f, transform.position.z);
        while (t <= 1.0f)
        {
            t += step; // Goes from 0 to 1, incrementing by step each time
            transform.position = Vector3.Lerp(a,b , t); // Move objectToMove closer to b
            yield return waitForFixedUpdate;            // Leave the routine and return here in the next frame
        }
        transform.position = new Vector3();

    }
}
