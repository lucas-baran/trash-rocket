using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryCinematic : MonoBehaviour
{
    [SerializeField] private GameObject rocket;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject rocketFlames;
    [SerializeField] private float playerSpeed = 10;
    [SerializeField] private float rocketSpeed = 5;
    [SerializeField] private GameObject victoryCanvas;

    private Vector2 rocketPosition;

    private void Start()
    {
        rocketPosition = rocket.transform.position;
        Destroy(player.GetComponent<Rigidbody2D>());
    }

    private void Update()
    {
        if(Vector2.Distance(rocketPosition, player.transform.position) > 0.1)
        {
            player.transform.position = Vector2.MoveTowards(player.transform.position, rocketPosition, playerSpeed * Time.deltaTime);
        }
        else
        {
            player.SetActive(false);
            StartCoroutine(rocketCoroutine());
        }
    }

    private IEnumerator rocketCoroutine()
    {
        bool onScreen = true;
        rocketFlames.SetActive(true);
        while(onScreen)
        {
            rocket.transform.position += new Vector3(0, 1, 0) * Time.deltaTime * rocketSpeed;
            rocketFlames.transform.position += new Vector3(0, 1, 0) * Time.deltaTime * rocketSpeed;
            yield return null;

            if (rocketFlames.transform.position.y > 10) onScreen = false;
        }
        victoryCanvas.SetActive(true);
    }
}
