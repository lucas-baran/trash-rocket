using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float xMovementSpeedMultiplier = 2;
    [SerializeField]
    private float yMovementSpeedMultiplier = 2;
    [SerializeField]
    private float timeAnimation = 0.3f;

    [SerializeField]
    private PlayerInventory playerInventory;

    [Header("Sounds")]
    [SerializeField] private AudioSource takingScrapAudioSource;

    public Sprite playerSpriteRightOther;
    public Sprite playerSpriteLeftOther;
    public Sprite playerSpriteRight;
    public Sprite playerSpriteLeft;
    public Sprite playerSpriteUp;
    public Sprite playerSpriteDown;
    private SpriteRenderer playerRenderer;

    //inputs
    private float xAxisInput;
    private float yAxisInput;
    private bool interaction;
    private bool canPlay = false;
    private bool pause = false;
    private bool canPause = false;
    private bool restart;
    private bool skipedTuto = false;
    private float animationTimer = 0;
    private bool animatedPhase1;

    //components
    private Rigidbody2D playerRigidbody;
    private Collider2D playerCollider;
    private GameObject smokeEffect;
    private TutoManager tutoManager;
    private GameManager gameManager;

    void Start()
    {
        playerRigidbody = transform.GetComponent<Rigidbody2D>();
        smokeEffect = transform.Find("Particle System").gameObject;
        tutoManager = GameObject.Find("TutoCanvas").GetComponent<TutoManager>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        playerRenderer = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        GetInputs();
        if (canPlay)
            MovePlayer();
        if (canPause)
            CheckPause(pause);
        UpdatePlayerModel();
        CheckRestart();
    }


    private void GetInputs()
    {
        if (canPlay)
        {
            xAxisInput = Input.GetAxis("Horizontal");
            yAxisInput = Input.GetAxis("Vertical");
            interaction = Input.GetButton("Interaction");
        }
        if (!tutoManager.IsTuto())
        {
            if (!skipedTuto) pause = Input.GetButtonDown("Pause");
            else skipedTuto = false;
        }
        restart = Input.GetButtonDown("Restart");
    }

    private void MovePlayer()
    {
        float xResultantInput = xAxisInput* xMovementSpeedMultiplier * Time.fixedDeltaTime;
        float yResultantInput = yAxisInput * yMovementSpeedMultiplier * Time.fixedDeltaTime;
        Vector2 Res = new Vector2(xResultantInput, yResultantInput) / ( (xAxisInput != 0 && yAxisInput != 0) ? Mathf.Sqrt(2) : 1);
        playerRigidbody.MovePosition(playerRigidbody.position + Res);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Killer"))
        {
            playerInventory.loseScrap();
        }
        if (collision.CompareTag("Trash"))
        {
            collision.GetComponent<SpriteRenderer>().material.SetFloat("_EffectStrength", 3);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Trash"))
        {
            collision.GetComponent<SpriteRenderer>().material.SetFloat("_EffectStrength", 0);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Trash"))
        {
            if (interaction)
            {
                Trash trash = collision.GetComponent<Trash>();
                
                if (playerInventory.canPickScrap())
                {
                    
                    if (playerInventory.addScrapToInventory(trash.PickScrap()))
                    {
                        takingScrapAudioSource.Play();
                    }

                    if (tutoManager.IsTuto() && (tutoManager.getCurrentTuto() == 4 || tutoManager.getCurrentTuto() == 5) )
                        tutoManager.setPassToTrue();
                }
            }

        }
        if (collision.CompareTag("Rocket"))
        {
            if (interaction)
            {
                if (tutoManager.IsTuto())
                {
                    tutoManager.setPassToTrue();
                    playerInventory.PickAllScraps();
                    return;
                }

                collision.GetComponent<Rocket>().AddScraps(playerInventory.PickAllScraps());

                if (tutoManager.IsTuto() && (tutoManager.getCurrentTuto() == 4 || tutoManager.getCurrentTuto() == 5))
                    tutoManager.setPassToTrue();
            }

        }
    }

    public void setPlay(bool playing)
    {
        canPlay = playing;
    }

    private void CheckPause(bool pauseGame)
    {

        if (pauseGame && canPlay)
        {
            canPlay = false;
            gameManager.setPause(true);
        }
        else if (pauseGame && !canPlay)
        {
            canPlay = true;
            gameManager.setPause(false);
        }
    }

    private void UpdatePlayerModel()
    {
        if (yAxisInput != 0)
        {
            animationTimer += Time.deltaTime;
            if (animationTimer > timeAnimation)
            {
                playerRenderer.flipX = !playerRenderer.flipX;
                animationTimer = 0;
            }

            if (Mathf.Sign(yAxisInput) == 1)
            {
                playerRenderer.sprite = playerSpriteUp;
            }
            else playerRenderer.sprite = playerSpriteDown;
        }
        else if (xAxisInput != 0)
        {
            animationTimer += Time.deltaTime;
            if (animationTimer > timeAnimation)
            {
                animatedPhase1 = !animatedPhase1;
                animationTimer = 0;
            }

            playerRenderer.flipX = false;
            if (Mathf.Sign(xAxisInput) == 1)
            {
                playerRenderer.sprite = (animatedPhase1 ? playerSpriteRight : playerSpriteRightOther);
            }
            else playerRenderer.sprite = (animatedPhase1 ? playerSpriteLeft  : playerSpriteLeftOther);
        }
        if ( (Mathf.Abs(xAxisInput) +  Mathf.Abs(yAxisInput)) != 0)
        {
            smokeEffect.SetActive(true);
        }
        else
            smokeEffect.SetActive(false);
    }
    
    public void endTuto(bool skiped)
    {
        canPause = true;
        skipedTuto = skiped;
    }

    private void CheckRestart()
    {
        if (restart)
        {
            GameObject.Find("UI").GetComponent<MenuManager>().launchGame();
        }
    }
}