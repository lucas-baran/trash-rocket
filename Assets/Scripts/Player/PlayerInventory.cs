using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    [SerializeField]
    int inventorySize;

    [SerializeField] Canvas inventoryUI;
    [SerializeField] Image scrapImage1;
    [SerializeField] Image scrapImage2;
    [SerializeField] Image scrapImage3; //3 is considered max size for now
    private Animator inventoryUIAnimator;

    private List<Scrap> playerScraps;
    private List<Image> scrapImages;

    private void Start()
    {
        inventoryUIAnimator = inventoryUI.GetComponent<Animator>();

        scrapImages = new List<Image>();
        playerScraps = new List<Scrap>();

        scrapImages.Add(scrapImage1);
        scrapImages.Add(scrapImage2);
        scrapImages.Add(scrapImage3);

        updateInventoryUI();
    }

    public bool canPickScrap()
    {
        return playerScraps.Count < inventorySize;
    }

    public bool addScrapToInventory(Scrap scrap)
    {
        if (playerScraps.Count < inventorySize)
        {
            if (scrap == null) return false;
            playerScraps.Add(scrap);
        }
        else
        {
            //some feedback
            Debug.Log("inventory full");
            return false;
        }
        updateInventoryUI();
        return true;
    }

    public Scrap[] PickAllScraps()
    {
        Scrap[] scraps = playerScraps.ToArray();
        playerScraps.Clear();
        updateInventoryUI();
        return scraps;
    }

    //If player gets hit by a truck
    public void loseScrap()
    {
        playerScraps.Clear();
        //some feedback
        updateInventoryUI();
    }

    private void updateInventoryUI()
    {
        if (playerScraps.Count == 0)
        {
            inventoryUIAnimator.SetBool("Appear", false);
            Debug.Log("inventory empty");
            return;
        }

        scrapImages.ForEach(image => { image.enabled = false; }) ;

        for (int i = 0; i < playerScraps.Count; i++ )
        {
            scrapImages[i].sprite = playerScraps[i].Sprite;
            scrapImages[i].enabled = true;

        }

        inventoryUIAnimator.SetBool("Appear", true);
    }
}
