
public enum ScrapTier
{
    Normal,
    Rare,
    Epic,
    Legendary
}
