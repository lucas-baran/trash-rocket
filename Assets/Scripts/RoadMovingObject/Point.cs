using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    [SerializeField] private List<Point> connectedPoints = new List<Point>();
    [SerializeField] private Trash trash = null;

    public bool HasTrash { get => trash != null; }
    public Trash Trash { get => trash; }

    public Vector3 Position { get => transform.position; }

    public Point this[int i]
    {
        get { return connectedPoints[i]; }
    }

    private void Start()
    {
        foreach (Point otherPoint in connectedPoints)
        {
            if (!otherPoint.connectedPoints.Contains(this))
            {
                otherPoint.connectedPoints.Add(this);
            }
        }
    }

    public Point GetRandomConnectedPoint()
    {
        if (connectedPoints.Count == 0)
        {
            return null;
        }

        return connectedPoints[Random.Range(0, connectedPoints.Count)];
    }

    public Point GetRandomConnectedPointOtherThan(Point point)
    {
        if (connectedPoints.Count <= 1)
        {
            return null;
        }

        List<Point> connectedPointsCopy = new List<Point>(connectedPoints.Count);
        for (int i = 0; i < connectedPoints.Count; i++)
        {
            if (connectedPoints[i].Position != point.Position)
            {
                connectedPointsCopy.Add(connectedPoints[i]);
            }
        }

        return connectedPointsCopy[Random.Range(0, connectedPointsCopy.Count)];
    }


    #region Gizmos

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        foreach (Point otherPoint in connectedPoints)
        {
            if (otherPoint != null) Gizmos.DrawLine(Position, otherPoint.Position);
        }

        Gizmos.color = trash == null ? Color.red : Color.blue;
        Gizmos.DrawSphere(Position, 0.1f);
    }

    #endregion
}
