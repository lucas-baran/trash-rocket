using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageTruck : MonoBehaviour
{
    [SerializeField] private Point startPoint;
    [SerializeField] private float speed = 2f;
    [SerializeField] private float waitTimeAtEachPoint = 1f;
    [SerializeField] private float trashPickTime = 1f;
    
    [Header("Sounds")]
    [SerializeField] private AudioSource truckEngineAudioSource;
    [SerializeField] private AudioSource truckPickUpTrashAudioSource;
    [SerializeField] private float minDistanceVolume = 1f;
    [SerializeField] private float maxDistanceVolume = 5f;
    [SerializeField] private Transform soundTarget;
    private float truckEngineVolume;
    private float truckPickUpTrashVolume;

    private Point previousPoint;
    private Point targetPoint;
    private WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
    private WaitForSeconds waitForWaitTime;
    private WaitForSeconds waitForPickTime;
    private WaitWhile waitWhilePaused;
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        waitWhilePaused = new WaitWhile(() => gameManager.Pause);

        waitForWaitTime = new WaitForSeconds(waitTimeAtEachPoint);
        waitForPickTime = new WaitForSeconds(trashPickTime);
        transform.position = startPoint.Position;
        previousPoint = startPoint;
        targetPoint = startPoint.GetRandomConnectedPointOtherThan(startPoint);

        // Sounds
        truckEngineVolume = truckEngineAudioSource.volume;
        truckPickUpTrashVolume = truckPickUpTrashAudioSource.volume;
        truckPickUpTrashAudioSource.loop = false;
        truckEngineAudioSource.loop = true;
        truckEngineAudioSource.Play();

        StartCoroutine(TravelingCoroutine());
    }

    private void Update()
    {
        UpdateSoundsVolume();
    }

    private void UpdateSoundsVolume()
    {
        float dist = Vector2.Distance(transform.position, soundTarget.position);

        if (dist < minDistanceVolume)
        {
            truckEngineAudioSource.volume = truckEngineVolume;
            truckPickUpTrashAudioSource.volume = truckPickUpTrashVolume;
        }
        else if (dist > maxDistanceVolume)
        {
            truckEngineAudioSource.volume = 0f;
            truckPickUpTrashAudioSource.volume = 0f;
        }
        else
        {
            truckEngineAudioSource.volume = truckEngineVolume * (1 - ((dist - minDistanceVolume) / (maxDistanceVolume - minDistanceVolume)));
            truckPickUpTrashAudioSource.volume = truckPickUpTrashVolume * (1 - ((dist - minDistanceVolume) / (maxDistanceVolume - minDistanceVolume)));
        }
    }


    #region Coroutines

    private IEnumerator TravelingCoroutine()
    {
        while (true)
        {
            Vector3 direction = (targetPoint.Position - transform.position).normalized;
            transform.right = -direction;
            while ((transform.position - targetPoint.Position).sqrMagnitude > Time.fixedDeltaTime * speed * Time.fixedDeltaTime * speed)
            {
                yield return waitForFixedUpdate;
                yield return waitWhilePaused;

                transform.position += Time.fixedDeltaTime * speed * direction;
            }

            yield return waitForFixedUpdate;
            transform.position = targetPoint.Position;

            if (targetPoint.HasTrash)
            {
                truckPickUpTrashAudioSource.Play();
                yield return waitForPickTime;
                targetPoint.Trash.PickScrap();
            }
            else
            {
                yield return waitForWaitTime;
            }

            Point newPoint = targetPoint.GetRandomConnectedPointOtherThan(previousPoint);
            previousPoint = targetPoint;
            targetPoint = newPoint;
        }
    }

    #endregion
}
