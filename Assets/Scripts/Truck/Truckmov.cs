using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Truckmov : MonoBehaviour
{
    public GameObject pointD;
    public GameObject pointA;
    public float speed = 2;
    private float alleretour = 0;

    private SpriteRenderer truckRenderer;
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        truckRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (transform.position == pointD.transform.position)
        {
            alleretour = 0;
            truckRenderer.flipX = false;

        }
        else if (transform.position == pointA.transform.position)
        {
            alleretour = 1;
            truckRenderer.flipX = true;
            
        }

        if (!gameManager.Pause)
        {
            if (alleretour == 0)
            {
                transform.position = Vector2.MoveTowards(transform.position, pointA.transform.position, speed * Time.deltaTime);
            }
            else if (alleretour == 1)
            {
                transform.position = Vector2.MoveTowards(transform.position, pointD.transform.position, speed * Time.deltaTime);
            }
        }
    }
}
